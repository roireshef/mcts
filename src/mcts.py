import math

from mcts.src.environment import Environment, Model, Reward
from mcts.src.node import StateNode, ActionNode
from mcts.src.operators.backup import Backup
from mcts.src.operators.default_policy import DefaultPolicy
from mcts.src.operators.tree_policy import TreePolicy
from mcts.src.utils import Utils


class MCTS:
    def __init__(self, tree_policy: TreePolicy, default_policy: DefaultPolicy, backup: Backup):
        """"""
        self.backup = backup
        self.default_policy = default_policy
        self.tree_policy = tree_policy

    def search(self, env: Environment, iterations: int = 1000, max_depth: int = math.inf, max_simulation_depth: int = 50):
        """"""
        root = StateNode(parent=None, state=env.init_state, r=0, action_space=env.action_space)

        iter = 0
        while iter < iterations:
            leaf_node = self._select_and_expand(root, env, max_depth)
            simulated_reward = self._simulate_rollout(leaf_node, env.model, max_simulation_depth)
            self._update(leaf_node, simulated_reward)
            iter += 1

        return root

    def _select_and_expand(self, state_node: StateNode, env: Environment, max_depth: int) -> (StateNode, Reward):
        current_state_node = state_node

        while not current_state_node.state.is_terminal and current_state_node.depth < max_depth:
            best_action_node = self.tree_policy(current_state_node)
            next_state, immediate_reward = env.model.sample(current_state_node.state, best_action_node.action)

            if next_state in best_action_node.children.keys():
                current_state_node = best_action_node.children[next_state]
            else:
                new_state_node = StateNode(best_action_node, next_state, immediate_reward, env.action_space)
                best_action_node.children[next_state] = new_state_node
                return new_state_node

        return current_state_node

    def _simulate_rollout(self, state_node: StateNode, model: Model, max_simulation_depth: int) -> Reward:
        current_state = state_node.state

        simulated_reward = 0
        i = 0
        while current_state and i < max_simulation_depth:
            default_action = self.default_policy(current_state)
            next_state, immediate_reward = model.sample(current_state, default_action)
            simulated_reward += immediate_reward
            current_state = next_state
            i += 1

        return simulated_reward

    def _update(self, leaf_node: StateNode, simulated_reward: Reward):
        Utils.add_mc_to_node(leaf_node, simulated_reward)

        current_state_node = leaf_node
        while current_state_node and current_state_node.parent and current_state_node.parent.parent:
            self.backup(current_state_node, simulated_reward)
            current_state_node = current_state_node.parent.parent
