from mcts.src.environment import Reward
from mcts.src.node import StateNode, ActionNode, Node


class Utils:
    @staticmethod
    def get_tuple_from_s_tag(state_node: StateNode) -> (StateNode, ActionNode, Reward, StateNode):
        return state_node.parent.parent, state_node.parent, state_node.r, state_node

    @staticmethod
    def add_mc_to_node(node: Node, reward: Reward):
        node.n += 1
        node.v = ((node.n - 1) * node.v + reward) / node.n
