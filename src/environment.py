from abc import abstractmethod, ABCMeta
from typing import List, NamedTuple, Dict

import six

Reward = float


@six.add_metaclass(ABCMeta)
class State:
    def __init__(self, is_terminal: bool):
        self.is_terminal = is_terminal

    @property
    @abstractmethod
    def id(self):
        pass

    @abstractmethod
    def __hash__(self):
        pass

    def __eq__(self, other):
        return isinstance(other, self.__class__) and other.id == self.id

    def __str__(self):
        return str(self.id)


@six.add_metaclass(ABCMeta)
class Action:
    @property
    @abstractmethod
    def id(self):
        pass

    @abstractmethod
    def __hash__(self):
        pass

    def __eq__(self, other):
        return isinstance(other, self.__class__) and other.id == self.id

    def __str__(self):
        return str(self.id)


@six.add_metaclass(ABCMeta)
class ActionSpace:
    @abstractmethod
    def get_actions(self, state: State) -> List[Action]:
        pass


@six.add_metaclass(ABCMeta)
class Model:
    @abstractmethod
    def sample(self, state: State, action: Action) -> (State, Reward):
        pass


Environment = NamedTuple("Environment", [('init_state', State), ('action_space', ActionSpace), ('model', Model)])
