from typing import Dict, Optional, TypeVar, List

from mcts.src.environment import State, Action, Reward, ActionSpace

NodeType = TypeVar('NodeType', bound='Node')
StateNodeType = TypeVar('StateNodeType', bound='StateNode')
ActionNodeType = TypeVar('ActionNodeType', bound='ActionNode')


class Node:
    def __init__(self, parent: NodeType, n: int, v: float, children: Dict):
        self.parent = parent
        self.n = n
        self.v = v
        self.children = children
        self.depth = self.parent.depth + 1 if self.parent else 0


class StateNode(Node):
    def __init__(self, parent: Optional[ActionNodeType], state: State, r: Reward, action_space: ActionSpace):
        super().__init__(parent=parent, n=0, v=0, children={})
        self.set_children(action_space.get_actions(state))
        self.state = state
        self.r = r

    def set_children(self, actions: List[Action]):
        self.children = {a.id: ActionNode(self, a) for a in actions}

    @property
    def untried_actions(self):
        return [a for a in self.children.values() if a.n == 0]

    @property
    def tried_actions(self):
        return [a for a in self.children.values() if a.n > 0]

    def __str__(self):
        return "StateNode(%s, v=%.4f, n=%d)" % (self.state, self.v, self.n)


class ActionNode(Node):
    def __init__(self, parent: StateNodeType, action: Action):
        super().__init__(parent=parent, n=0, v=0, children={})
        self.action = action

    def __str__(self):
        return "ActionNode(%s, v=%.4f, n=%d)" % (self.action, self.v, self.n)