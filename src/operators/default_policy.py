import random
from abc import abstractmethod, ABCMeta

import six

from mcts.src.environment import State, ActionSpace, Action


@six.add_metaclass(ABCMeta)
class DefaultPolicy:
    @abstractmethod
    def __call__(self, state: State) -> Action:
        pass


class RandomDefaultPolicy(DefaultPolicy):
    def __init__(self, action_space: ActionSpace):
        self.action_space = action_space

    def __call__(self, state: State) -> Action:
        actions = self.action_space.get_actions(state)
        return random.sample(actions, 1)[0]
