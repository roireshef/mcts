from abc import abstractmethod, ABCMeta

import six

from mcts.src.environment import Reward
from mcts.src.node import StateNode
from mcts.src.utils import Utils


@six.add_metaclass(ABCMeta)
class Backup:
    @abstractmethod
    def __call__(self, state_node: StateNode, rollout_reward: Reward) -> None:
        pass


class MonteCarloBackup(Backup):
    def __call__(self, state_node: StateNode, rollout_reward: Reward) -> None:
        s, a, r, _ = Utils.get_tuple_from_s_tag(state_node)

        Utils.add_mc_to_node(a, rollout_reward + r)
        Utils.add_mc_to_node(s, rollout_reward + r)


class BellmanBackup(Backup):
    def __call__(self, state_node: StateNode, rollout_reward: Reward) -> None:
        s, a, r, _ = Utils.get_tuple_from_s_tag(state_node)

        a.n += 1
        a.v = sum([(s_tag.r + s_tag.v) * s_tag.n for s_tag in a.children.values()]) / a.n

        s.n += 1
        s.v = max([a_i.v for a_i in s.tried_actions])
