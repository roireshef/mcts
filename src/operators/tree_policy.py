import math
import random
from abc import abstractmethod, ABCMeta
from typing import Callable

import six

from mcts.src.node import ActionNode, StateNode


@six.add_metaclass(ABCMeta)
class TreePolicy:
    @abstractmethod
    def __call__(self, state_node: StateNode) -> ActionNode:
        pass


class Greedy(TreePolicy):
    def __init__(self, eval_func: Callable[[StateNode, ActionNode], float]):
        self.eval_func = eval_func

    def __call__(self, state_node: StateNode) -> ActionNode:
        best_action_node = None
        best_q = None
        for action_node in state_node.children.values():
            q = self.eval_func(state_node, action_node)
            if (not best_q) or q > best_q:
                best_q = q
                best_action_node = action_node
        return best_action_node


class EpsilonGreedy(TreePolicy):
    def __init__(self, eps: float):
        self.eps = eps
        self.greedy_policy = Greedy(lambda s, a: a.v)

    def __call__(self, state_node: StateNode) -> ActionNode:
        if random.random() > self.eps:
            return random.sample(state_node.children.values(), 1)

        return self.greedy_policy(state_node)


class UCT(TreePolicy):
    def __init__(self, c_p: float):
        self.uct_policy = Greedy(eval_func=lambda s, a: a.v + 2 * c_p * math.sqrt(2 * math.log(s.n) / a.n))

    def __call__(self, state_node: StateNode) -> ActionNode:
        unexpanded = state_node.untried_actions
        if len(unexpanded) > 0:
            return random.sample(unexpanded, 1)[0]

        return self.uct_policy(state_node)

