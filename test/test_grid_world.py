import time

from mcts.src.mcts import MCTS
from mcts.src.node import ActionNode
from mcts.src.operators.backup import MonteCarloBackup, BellmanBackup
from mcts.src.operators.default_policy import RandomDefaultPolicy
from mcts.src.operators.tree_policy import UCT
from mcts.test.environments.grid_world import GridWorldEnvironment, Cell, GridCoordinate

grid = [
    [Cell.Empty,    Cell.Empty,    Cell.Empty,  Cell.Empty,     Cell.Blocked,   Cell.Terminal],
    [Cell.Empty,    Cell.Empty,    Cell.Empty,  Cell.Blocked,   Cell.Blocked,   Cell.Empty],
    [Cell.Empty,    Cell.Blocked,  Cell.Empty,  Cell.Empty,     Cell.Empty,     Cell.Empty],
    [Cell.Empty,    Cell.Blocked,  Cell.Empty,  Cell.Empty,     Cell.Blocked,   Cell.Blocked],
    [Cell.Empty,    Cell.Blocked,  Cell.Empty,  Cell.Empty,     Cell.Empty,     Cell.Empty],
]
grid.reverse()

env = GridWorldEnvironment.create(grid, GridCoordinate(0, 0), 1)

mcts = MCTS(tree_policy=UCT(c_p=0.3), default_policy=RandomDefaultPolicy(env.action_space), backup=MonteCarloBackup())

start = time.time()
tree = mcts.search(env, 1000, 30, 50)
end = time.time() - start

print('\nOptimal Plan: ')

node = tree
while node.children:
    if isinstance(node, ActionNode):
        print(node.action.id, end=' ')
    node = max(node.children.values(), key=lambda x: x.n)

print('\n\n=======================================\n')

node = tree
while node.children:
    print(node)
    node = max(node.children.values(), key=lambda x: x.n)

print('\n=======================================\n')
print('>>> GridWorld search ended in %.3f sec' % end)

