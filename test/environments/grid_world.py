import copy
import random
from enum import Enum
from typing import List, NamedTuple, Dict

from mcts.src.environment import State, Action, ActionSpace, Model, Reward, Environment


class Cell(Enum):
    Empty = 0
    Blocked = 1
    Terminal = 2


class Move(Enum):
    Up = 0
    Down = 1
    Left = 2
    Right = 3


GridCoordinate = NamedTuple('GridCoordinate', [('row', int), ('col', int)])
WorldGrid = List[List[Cell]]


class GridWorldState(State):
    def __init__(self, grid: WorldGrid, player_pos: GridCoordinate):
        super().__init__(grid[player_pos.row][player_pos.col] == Cell.Terminal)
        self.grid = grid
        self.player_pos = player_pos

    def __call__(self, coordinate: GridCoordinate):
        return self.grid[coordinate.row][coordinate.col]

    @property
    def id(self):
        return self.player_pos

    def __hash__(self):
        return self.player_pos.row * self.rows + self.player_pos.col

    @property
    def rows(self):
        return len(self.grid)

    @property
    def cols(self):
        return len(self.grid[0])


class GridWorldAction(Action):
    def __init__(self, move: Move):
        self.move = move

    @property
    def id(self):
        return self.move

    def __hash__(self):
        return self.move.value


class GridWorldActionSpace(ActionSpace):
    def get_actions(self, state: GridWorldState) -> List[GridWorldAction]:
        return [GridWorldAction(m) for m in Move if GridWorldModel.try_move(state, m) != state.player_pos]


class GridWorldModel(Model):
    def __init__(self, p_success: float):
        self.p_success = p_success

    def sample(self, state: GridWorldState, action: GridWorldAction) -> (State, Reward):
        if state.is_terminal:
            return None, 1
        elif random.random() < self.p_success:
            new_position = GridWorldModel.try_move(state, action.move)
        else:
            new_position = GridWorldModel.try_move(state, random.sample(list(Move), 1)[0])
        new_state = GridWorldState(state.grid, new_position)

        return new_state, -0.01

    @staticmethod
    def try_move(state: GridWorldState, move: Move) -> GridCoordinate:
        if move == Move.Down:
            new_player_pos = GridCoordinate(state.player_pos.row - 1, state.player_pos.col)
        elif move == Move.Up:
            new_player_pos = GridCoordinate(state.player_pos.row + 1, state.player_pos.col)
        elif move == Move.Left:
            new_player_pos = GridCoordinate(state.player_pos.row, state.player_pos.col - 1)
        else:  # move == Move.Right
            new_player_pos = GridCoordinate(state.player_pos.row, state.player_pos.col + 1)

        if 0 <= new_player_pos.row <= state.rows-1 and 0 <= new_player_pos.col <= state.cols-1 and \
                state(new_player_pos) != Cell.Blocked:
            return new_player_pos
        else:
            return state.player_pos


class GridWorldEnvironment(Environment):
    @staticmethod
    def create(grid: WorldGrid, init_pos: GridCoordinate, p_success: float):
        return Environment(GridWorldState(grid, init_pos), GridWorldActionSpace(), GridWorldModel(p_success))
