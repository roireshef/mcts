# Monte Carlo Tree Search and Interactive Environment Model

The contribution of this repo is 2-fold:

A) Generic [Monte Carlo Tree Search (MCTS)](https://en.wikipedia.org/wiki/Monte_Carlo_tree_search) implementation written in [python](https://www.python.org/).

B) Generic Environment Model for planning/learning tasks on an [MDP](https://en.wikipedia.org/wiki/Markov_decision_process) (a GridWorld example is included)


MCTS is a relatively simple tree search concept that aims to solve the exploration-exploitation problem (similar to Multi-Arm Bandit problem, but) of sequential nature, such as recursive traversal through a tree (e.g. MDP).
 
The suggested MCTS design is general in the sense that it can accomodate different operators (policies, backups, etc.) and in that it is straighforward to write custom operators and plug them into the framework.

Environmental Design:
-----
![picture](img/DataModel.png)

Roadmap:
-----
[X] Generic MCTS framework

[ ] Port to [PyTorch](https://pytorch.org/) / [Tensorflow](https://www.tensorflow.org/) for combining learning in the planning process (e.g [Reinforcement Learning](https://en.wikipedia.org/wiki/Reinforcement_learning))

[ ] Async MCTS Implementation

[ ] Operators for [zero-sum games](https://en.wikipedia.org/wiki/Zero-sum_game)

[ ] Operators for general-sum games

[ ] MCTS for self-play

[ ] Visualization & monitoring tools